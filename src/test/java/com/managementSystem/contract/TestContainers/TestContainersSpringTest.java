package com.managementSystem.contract.TestContainers;

import com.managementSystem.contract.dao.ContractRepository;
import com.managementSystem.contract.dto.ContractDto;
import com.managementSystem.contract.model.Contract;
import org.junit.Assert;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.TestPropertySourceUtils;
import org.testcontainers.containers.MariaDBContainer;

import java.util.HashMap;
import java.util.Map;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(initializers = TestContainersSpringTest.Init.class)
public class TestContainersSpringTest {

    @ClassRule
    public static MariaDBContainer container = new MariaDBContainer();

    @LocalServerPort //mapuje port z kontekstu springowego
    private int localPort;

    @Autowired
    private ContractRepository contractRepository;

    public TestRestTemplate testRestTemplate = new TestRestTemplate();

    @Autowired
    ApplicationContext context;

    @Test
    public void marieDbTest(){
        final Environment environment = context.getEnvironment();
        System.out.println(environment.getProperty("spring.datasource.url"));
        System.out.println(environment.getProperty("spring.datasource.username"));
        System.out.println(environment.getProperty("spring.datasource.password"));
        System.out.println(environment.getProperty("spring.datasource.driver-class-name"));
        Map<String, String> requestParam = new HashMap<String, String>();

        ContractDto requestBody = new ContractDto();
        requestBody.setValid(true);

        Assert.assertEquals(1, contractRepository.findAll().size());
//TODO Controller restowy dla contract na wzor systemu - ok
        ResponseEntity<ContractDto> result = testRestTemplate.postForEntity("http://localhost:" + localPort + "/api/contract",
                requestBody, ContractDto.class, requestParam);

        Assert.assertNotNull(result);


    }

    public static class Init implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        @Override
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {

              TestPropertyValues testPropertyValues = TestPropertyValues.of(
                      "spring.datasource.url=" + container.getJdbcUrl(),
                      "spring.datasource.username=" + container.getUsername(),
                      "spring.datasource.password=" + container.getPassword(),
                      "spring.datasource.driver-class-name=" + container.getDriverClassName());
            testPropertyValues.applyTo(configurableApplicationContext);

        }
    }
    //todo podmienić propertasy z h2 na mariaDB -ok


}
