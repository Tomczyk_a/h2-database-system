package com.managementSystem.contract.dao;

import com.managementSystem.contract.model.Contract;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import java.util.List;

import static com.managementSystem.contract.tools.ContractGenerator.toContract;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(org.mockito.junit.MockitoJUnitRunner.class)
public class ContractRepositoryTest {

    @Mock
    private ContractRepository contractRepository;

    @Test
    public void findAllContractsTest() {
        //given
        final List<Contract> all = contractRepository.findAll();
        //then
        Assert.assertEquals(all.size(), 0);

    }
    @Test
    public void saveContractTest() {
        //Given
        final Contract contract2 = toContract();
        when(contractRepository.save(contract2)).thenReturn(contract2);
        //When
        final Contract saved = contractRepository.save(contract2);
        //Then
        Assert.assertEquals(saved, contract2);
    }

    @Test
    public void deleteContractTest() {
        //given
        final Contract contract2 = toContract();
        when(contractRepository.save(contract2)).thenReturn(contract2);

        //when
        contractRepository.delete(contract2);
        //then
        verify(contractRepository).delete(contract2);

    }
    @Test
    public void updateContractTest(){
        //given
        final Contract contract2 = toContract();
        when(contractRepository.save(contract2)).thenReturn(contract2);

        //when
        contractRepository.saveAndFlush(contract2);
    }
}
