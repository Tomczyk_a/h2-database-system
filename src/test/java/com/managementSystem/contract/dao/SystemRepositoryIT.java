package com.managementSystem.contract.dao;

import com.managementSystem.contract.model.System;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.managementSystem.contract.tools.SystemGenerator.toSystem;
import static org.mockito.Mockito.when;

@SpringBootTest
@RunWith(SpringRunner.class)
public class SystemRepositoryIT {

    @Mock
    SystemRepository systemRepository;

    @Test
    public void findAllSystemsTest(){
        //given
        //when
        final List<System> all = systemRepository.findAll();
        //then
        Assert.assertEquals(all.size(), 0);
    }

    @Test
    public void findByIdTest(){
        //given
        final System system1 = toSystem();

        List<System> systemList = new ArrayList<>();
        systemList.add(system1);
        when(systemRepository.findById(1L)).thenReturn(Optional.of(system1));

        //when
        Optional<System> result = systemRepository.findById(1L);
        //then
        Assert.assertEquals(result, Optional.of(system1));
    }

    @Test
    public void saveSystemTest(){
        //given
        final System system1 = toSystem();
        when(systemRepository.save(system1)).thenReturn(system1);
        //when
        final System saved = systemRepository.save(system1);
        //then
        Assert.assertEquals(saved, system1);
    }

    @Test
    public void saveAndFindAllTest(){
        //given
        final System system1 = toSystem();
        when(systemRepository.save(system1)).thenReturn(system1);
        //when
        final List<System> all = systemRepository.findAll();
        //then
        Assert.assertEquals(all.size(), 1);
    }

}
