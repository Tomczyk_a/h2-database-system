package com.managementSystem.contract.dao;

import com.managementSystem.contract.model.Contract;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static com.managementSystem.contract.tools.ContractGenerator.toContract;
import static junit.framework.TestCase.fail;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ContractRepositoryH2IT {
    @Autowired
    ContractRepository contractRepository;

    @Test(expected = RuntimeException.class)
    public void save() {
        //given
        final Contract contract2 = toContract();
        //when
        final Contract saved = contractRepository.save(contract2);
        //then
//        we check every position separately - in case of failure we know exactly what went wrong
        Assert.assertEquals(contract2.getAmount(), saved.getAmount());
        Assert.assertEquals(contract2.getContractNumber(), saved.getContractNumber());
        Assert.assertEquals(contract2.getDateFrom(), saved.getDateFrom());
        Assert.assertEquals(contract2.getSystem(), saved.getSystem());
        //when
        final Contract contract = contractRepository
                .findById(saved.getContractId())
                .orElseThrow(() -> new RuntimeException("Where is my contract?"));
        //then
        Assert.assertEquals(saved, contract);
        //when
        contractRepository.delete(contract);
        //then
        contractRepository
                .findById(saved.getContractId())
                .orElseThrow(() -> new RuntimeException("Where is my contract?"));
        fail();

    }

    @Test
    public void delete() {
        //given
        final Contract contract2 = toContract();
        //when


    }
}
