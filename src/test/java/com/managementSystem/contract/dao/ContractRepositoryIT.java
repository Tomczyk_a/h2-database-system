package com.managementSystem.contract.dao;

import com.managementSystem.contract.model.Contract;
import com.managementSystem.contract.model.System;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.managementSystem.contract.tools.ContractGenerator.toContract;

//testy integracyjne
@SpringBootTest
@RunWith(SpringRunner.class)

//testy jednostkowe
//@RunWith(org.mockito.junit.MockitoJUnitRunner.class)

public class ContractRepositoryIT {

    @Autowired
    ContractRepository contractRepository;

    private System system1;

    //save, find
    //find, update, find
    //save, find, delete, find

    @Before
    public void before() {
        contractRepository.deleteAll();
    }

    @Test
    public void saveFindDeleteFindAllContractTest() {
        //Given empty repository
        contractRepository.deleteAll();
        final Contract contract1 = toContract();

        List<Contract> contractList = new ArrayList<>();
        contractList.add(contract1);
        //when
        final Contract saved = contractRepository.save(contract1);
        final List<Contract> all = contractRepository.findAll();
        //then
        Assert.assertEquals(all.size(), 1);
        //when
        contractRepository.delete(saved);
        //then
        final int size = contractRepository
                .findAll()
                .size();
        Assert.assertEquals(size, 0);
    }

    @Test
    public void findAllContractTest() {
        //Given
        //when
        final List<Contract> all = contractRepository.findAll();
        //then
        Assert.assertEquals(all.size(), 0);
    }

//    @Test
//    public void saveContractTest() {
//        //Given
//        final Contract contract2 = toContract();
//
//        //When
//        final Contract saved = contractRepository.save(contract2);
//        //Then
//        Assert.assertEquals(); //todo porownajmy cos innego
//    }

    @Test
    public void findByIdTest() {
        //given
        final Contract contract2 = toContract();

        List<Contract> contractList = new ArrayList<>();
        contractList.add(contract2);
        final Contract saved = contractRepository.save(contract2);
//todo save

        //when
        Optional<Contract> result = contractRepository.findById(saved.getContractId());//TODO zamiast 8, id zapisanego
        //then
        Assert.assertEquals(result, Optional.of(contract2));

    }

    @Test
    public void saveAndFindAllContractTest() {
        //Given
        final Contract contract2 = toContract();

        //when
        final Contract saved = contractRepository.save(contract2);

        final List<Contract> all = contractRepository.findAll();
        //then
        Assert.assertEquals(all.size(), 1);
    }

    @Test
    public void findUpdateFindAllContractTest() {
        //Given
        //when
        final List<Contract> all = contractRepository.findAll();
        //then
        Assert.assertEquals(all.size(), 0);
        //when
        final Contract contract1 = toContract();
        all.add(contract1);
        //then
        Assert.assertEquals(all.size(), 1);
        //TODO repository
    }


}