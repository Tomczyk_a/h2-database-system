package com.managementSystem.contract.tools;

import com.managementSystem.contract.model.System;

public class SystemGenerator {

    public SystemGenerator() {
    }
    public static System toSystem() {
        return System.builder()
                .name("My Company")
                .systemId(1L)
                .description("description")
                .client("Big Client")
                .technologyDescription(" interesting things")
                .build();
    }

}
