package com.managementSystem.contract.tools;

import com.managementSystem.contract.model.Contract;
import com.managementSystem.contract.model.PayOff;

import java.math.BigDecimal;
import java.time.LocalDate;
// method moved to separate class - we've done this for save time and for better readability of the code in test classes
public class ContractGenerator {
//thanks constructor nobody can create that object in different place
    private ContractGenerator() {
    }
// thanks builder we have got better readability of the code
    public static Contract toContract() {
        return Contract.builder()
                .payOff(PayOff.ANNUAL)
                .amount(BigDecimal.TEN.setScale(2))
                .dateTo(LocalDate.now())
                .dateFrom(LocalDate.of(1990, 7, 2))
                .valid(true)
                .contractId(1L)
                .contractNumber(6565656)
                .build();
    }
}
