package com.managementSystem.contract.service;

import com.managementSystem.contract.controller.TaskNotFoundException;

import java.util.List;


public interface ServiceAPI<T>{

     List<T> findAll();
     T findById( Long id) throws TaskNotFoundException;
     T create(T t);
     T update(T t);
     T delete(T t);

}
