package com.managementSystem.contract.service;

import com.managementSystem.contract.controller.TaskNotFoundException;
import com.managementSystem.contract.dao.ContractRepository;
import com.managementSystem.contract.dto.ContractDto;
import com.managementSystem.contract.mapper.ContractMapper;
import com.managementSystem.contract.model.Contract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class ContractService implements ServiceAPI<ContractDto> {

    @Autowired
    ContractRepository contractRepository;

    @Override
    public List<ContractDto> findAll() {
        final List<Contract> all = contractRepository.findAll();
        return all
                .stream()
                .map(contract -> ContractMapper.INSTANCE.contractToContractDto(contract))
                .collect(Collectors.toList());
    }

    @Override
    public ContractDto findById(Long id) throws TaskNotFoundException {

//        final Contract contract = contractRepository.findById(id).orElseThrow(TaskNotFoundException::new);
//        return ContractMapper.INSTANCE.contractToContractDto(contract);

        Optional<Contract> contract1 = contractRepository.findById(id);
        if (!contract1.isPresent()) {
            throw new TaskNotFoundException();
        }
        return ContractMapper.INSTANCE.contractToContractDto(contract1.get());
    }

    @Override
    public ContractDto create(ContractDto contractDto) {
        final Contract contract = ContractMapper.INSTANCE.contractDtoToContract(contractDto);
        contractRepository.save(contract);
        return contractDto;
    }

    @Override
    public ContractDto update(ContractDto contractDto) {
        Contract contract = ContractMapper.INSTANCE.contractDtoToContract(contractDto);
        contractRepository.save(contract);
        return contractDto;
    }

    @Override
    public ContractDto delete(ContractDto contractDto) {
        Contract contract = ContractMapper.INSTANCE.contractDtoToContract(contractDto);
        contractRepository.delete(contract);
        return contractDto;
    }
}
