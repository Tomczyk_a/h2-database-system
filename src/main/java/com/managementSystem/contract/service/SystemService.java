package com.managementSystem.contract.service;

import com.managementSystem.contract.controller.TaskNotFoundException;
import com.managementSystem.contract.dao.SystemRepository;
import com.managementSystem.contract.dto.SystemDto;
import com.managementSystem.contract.errorcodes.SystemNotFoundException;
import com.managementSystem.contract.mapper.SystemMapper;
import com.managementSystem.contract.model.System;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class SystemService implements ServiceAPI<SystemDto>  {

    private static final Logger LOGGER = LoggerFactory.getLogger(System.class);

    private SystemRepository systemRepository;

    @Autowired
    public SystemService(SystemRepository systemRepository) {
        this.systemRepository = systemRepository;
    }

    @Override
    public List<SystemDto> findAll() {
        final List<System> all = systemRepository.findAll();
        return all
                .stream()
                .map(system -> SystemMapper.INSTANCE.systemToSystemDto(system))
                .collect(Collectors.toList());
    }

    //TODO sprobuj napisac streamowo
    @Override
    public SystemDto findById(Long id){
        LOGGER.info("preparing to log in");
        System system = null;
        try {
            system = systemRepository
                    .findById(id).orElseThrow(() -> new TaskNotFoundException());
        } catch (TaskNotFoundException e) {
            LOGGER.error(e.getMessage());
            throw new SystemNotFoundException("can not find system with Id" + id);
        }
        return SystemMapper.INSTANCE.systemToSystemDto(system);
    }


//    @Override
//    public SystemDto findById(Long id) throws TaskNotFoundException {
//        final Optional<System> byId = systemRepository.findById(id);
//        LOGGER.info("preparing to log in");
//        if (!byId.isPresent()){
//            LOGGER.warn("Very useful information - you've got a problem ");
//            //TODO logowanie bledu
//            throw new TaskNotFoundException();
//        }
//        return SystemMapper.INSTANCE.systemToSystemDto(byId.get());
//    }


    @Override
    public SystemDto create(SystemDto systemDto) {
        final System system = SystemMapper.INSTANCE.systemDtoToSystem(systemDto);
        systemRepository.save(system);
        return systemDto;
    }

    @Override
    public SystemDto update(SystemDto systemDto) {
        final System system = SystemMapper.INSTANCE.systemDtoToSystem(systemDto);
        systemRepository.save(system);
        return systemDto;
    }

    @Override
    public SystemDto delete(SystemDto systemDto) {
        System system = SystemMapper.INSTANCE.systemDtoToSystem(systemDto);
        systemRepository.delete(system);
        return systemDto;
    }
}
