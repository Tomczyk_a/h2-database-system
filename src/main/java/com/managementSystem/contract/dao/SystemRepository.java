package com.managementSystem.contract.dao;

import com.managementSystem.contract.model.System;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SystemRepository extends JpaRepository<System, Long> {
}
