package com.managementSystem.contract.dao;

import com.managementSystem.contract.model.Contract;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContractRepository extends JpaRepository <Contract, Long>{
}
