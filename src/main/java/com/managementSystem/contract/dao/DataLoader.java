package com.managementSystem.contract.dao;

import com.managementSystem.contract.model.Contract;
import com.managementSystem.contract.model.System;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.time.LocalDate;

//TODO https://mockaroo.com/ --> zapisac do pliku schema.sql schemat bazodanowy wygenerowany z tej strony. - ok
//TODO Nalezy wygenerowac inserty dla tabeli System oraz dla tabeli Contract - ok
//TODO Rozszerz ControllerAPI o dodatkowe metody -ok
@Component
public class DataLoader {

    private static final Logger LOGGER = LoggerFactory.getLogger(DataLoader.class);


    ContractRepository contractRepository;
    SystemRepository systemRepository;
    private Boolean mocked;

    @Autowired
    public DataLoader(ContractRepository contractRepository,
                      SystemRepository systemRepository,
                      @Value("${mock.data}") Boolean mocked) {
        this.contractRepository = contractRepository;
        this.systemRepository = systemRepository;
        this.mocked = mocked;

    }
    //TODO autowired przez constructor - ok

    //TODO uzaleznij wykonanie metody init od propertasa zapisanego w application.properties @Value -  ok

    @PostConstruct
    public void init() {
//        final System system = System.builder().client("3456").description("4567").systemId(3456L).name("4567").build();
        if (mocked) {
            LOGGER.info("mockdata = true");
//            final System system = System.builder().client("3456").description("4567").systemId(3456L).name("4567").build();
//            systemRepository.save(system);
//            final Contract contract = Contract.builder().amount(BigDecimal.valueOf(5485)).valid(true).dateTo(LocalDate.now()).build();
//            contractRepository.save(contract);
            //todo fix it - builder doesnt work
        } else {
            LOGGER.info("mockdata = false");
        }


    }
//   TODO stworzyć schemat z uzyciem mookaro i zapisać w pliku schema-h2.sql - ok
//   TODO uzyć mappera we wszystkich metodach serwisu - ok
//   TODO poprawka insertu dla systemu - identyfikator - ok
}
