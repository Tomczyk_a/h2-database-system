package com.managementSystem.contract.controller;

import com.managementSystem.contract.dto.SystemDto;
import com.managementSystem.contract.errorcodes.SystemNotFoundException;
import com.managementSystem.contract.service.SystemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import java.util.List;

@Controller
public class SystemController implements ControllerAPI<SystemDto> {
    @Autowired
    SystemService systemService;

    @Override
    public String findAll(Model model, SystemDto systemDto) {
        final List<SystemDto> all = systemService.findAll();
        model.addAttribute("systems", all);
        return "systemList";
    }

    @Override
    public String findOne(Model model, SystemDto systemDto) {
        try {
            final SystemDto systemDto1 = systemService.findById(systemDto.getSystemId());
            model.addAttribute("system", systemDto1);
        } catch (SystemNotFoundException e) {
            e.printStackTrace();
        }
        return "system";
    }

    @Override
    public String create(Model model, SystemDto systemDto) {
        final SystemDto systemDto1 = systemService.create(systemDto);
        model.addAttribute("sys", systemDto1);
        return "sys";
    }

    @Override
    public String update(Model model, SystemDto systemDto) {
        final SystemDto update = systemService.update(systemDto);
        model.addAttribute("sys", update);
        return "sys";
    }

    @Override
    public String delete(Model model, SystemDto systemDto) {
        final SystemDto delete = systemService.delete(systemDto);
        model.addAttribute("sys", delete);
        return "sys";
    }
    
}
