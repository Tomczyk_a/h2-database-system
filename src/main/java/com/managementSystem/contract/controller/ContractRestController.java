package com.managementSystem.contract.controller;

import com.managementSystem.contract.dto.ContractDto;
import com.managementSystem.contract.service.ContractService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;
import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.POST;



@RestController
public class ContractRestController {

    private ContractService contractService;
    private static final Logger LOGGER = LoggerFactory.getLogger(Logger.class);

    @Autowired
    public ContractRestController(ContractService contractService) {
        this.contractService = contractService;
    }
    @GetMapping("/contract")
    public List<ContractDto> findAll() {
        return contractService.findAll();
    }
    @GetMapping("/contract/{contractId}")
    public ContractDto find(@PathVariable Long contractId) throws TaskNotFoundException {
                return contractService.findById(contractId);
    }

    @PutMapping("/contract")
    public ContractDto update(@RequestBody ContractDto contractDto) {
        return contractService.update(contractDto);
    }

    @DeleteMapping("/contract/{contractId}")
    public void delete(@PathVariable Long contractId) throws TaskNotFoundException {
        final ContractDto contractDto= contractService.findById(contractId);
        contractService.delete(contractDto);
    }
    @RequestMapping(path = "/contract",
            method = POST,
            consumes = APPLICATION_JSON_VALUE)
    public ContractDto save(@RequestBody ContractDto c){
        return c;
    }
}
