package com.managementSystem.contract.controller;

import org.springframework.ui.Model;

public interface ControllerAPI <T>{
    String findAll(Model model, T t) throws TaskNotFoundException;
    String findOne(Model model, T t);
    String create(Model model, T t);
    String update(Model model, T t);
    String delete(Model model, T t);




}
