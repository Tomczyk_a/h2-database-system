package com.managementSystem.contract.controller;

import com.managementSystem.contract.dto.ContractDto;
import com.managementSystem.contract.service.ContractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import java.util.List;

@Controller
public class ContractController implements ControllerAPI<ContractDto>{
    @Autowired
    ContractService contractService;

    @Override
    public String findAll(Model model, ContractDto contractDto) {
        final List<ContractDto> all = contractService.findAll();
        model.addAttribute("contracts", all);
        return "contractList";
    }

    @Override
    public String findOne(Model model, ContractDto contractDto) {
        try {
            final ContractDto byId = contractService.findById(contractDto.getContractId());
            model.addAttribute("contract", byId );
        } catch (TaskNotFoundException e) {
            e.printStackTrace();
        }
        return "contrct";
    }

    @Override
    public String create(Model model, ContractDto contractDto) {
        final ContractDto contractDto1 = contractService.create(contractDto);
        model.addAttribute("cont", contractDto1);
        return "cont";
    }

    @Override
    public String update(Model model, ContractDto contractDto) {
        final ContractDto update = contractService.update(contractDto);
        model.addAttribute("cont", update);
        return "cont";
    }

    @Override
    public String delete(Model model, ContractDto contractDto) {
        final ContractDto delete = contractService.delete(contractDto);
        model.addAttribute("cont", delete);
        return "cont";
    }

}
