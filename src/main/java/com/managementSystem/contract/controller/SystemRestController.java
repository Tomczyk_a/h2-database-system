package com.managementSystem.contract.controller;

import com.managementSystem.contract.dto.SystemDto;
import com.managementSystem.contract.errorcodes.SystemNotFoundException;
import com.managementSystem.contract.service.SystemService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
public class SystemRestController {

    private SystemService systemService;
    private static final Logger LOGGER = LoggerFactory.getLogger(Logger.class);

    @Autowired
    public SystemRestController(SystemService systemService) {
        this.systemService = systemService;
    }

    @GetMapping("/system")
    public List<SystemDto> findAll() {
        return systemService.findAll();
    }
    @GetMapping("/system/{systemId}")
    public SystemDto find(@PathVariable Long systemId) {
        return systemService.findById(systemId);
    }


    @PutMapping("/system")
    public SystemDto update(@RequestBody SystemDto systemDto) {
        return systemService.update(systemDto);
    }

    @DeleteMapping("/system/{systemId}")
    public void delete(@PathVariable Long systemId) throws SystemNotFoundException {
        LOGGER.info("deleted" + systemId);
            final SystemDto systemDto = systemService.findById(systemId);
            systemService.delete(systemDto);
    }

    @RequestMapping(path = "/system",
            method = POST,
            consumes = APPLICATION_JSON_VALUE)
    public SystemDto save(@RequestBody SystemDto s) {
        systemService.create(s);
        return s;
    }


    // TODO Dodawanie,Modyfikowanie,Usuwanie systemow poprzez metody restowe - ok

}
