package com.managementSystem.contract.mapper;

import com.managementSystem.contract.dto.SystemDto;
import com.managementSystem.contract.model.System;
import org.mapstruct.Mapper;


import org.mapstruct.factory.Mappers;

@Mapper
public interface SystemMapper {
    SystemMapper INSTANCE= Mappers.getMapper(SystemMapper.class);

    System systemDtoToSystem(SystemDto systemDto);

    SystemDto systemToSystemDto(System system);
}
