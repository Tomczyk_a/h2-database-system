package com.managementSystem.contract.mapper;

import com.managementSystem.contract.dto.ContractDto;
import com.managementSystem.contract.model.Contract;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface ContractMapper {
    ContractMapper INSTANCE = Mappers.getMapper(ContractMapper.class);

//    @Mapping(target = "categoryDtos", source = "categories")
    ContractDto contractToContractDto(Contract contract);

//    @Mapping(target = "categories", source = "categoryDtos")
    Contract contractDtoToContract(ContractDto contractDto);
}
