package com.managementSystem.contract.dto;

import com.managementSystem.contract.model.PayOff;
import com.managementSystem.contract.model.System;
import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDate;

@NoArgsConstructor
@Data
@Getter
@AllArgsConstructor
@Builder

public class ContractDto {
    private Long contractId;
    private Integer contractNumber;
    private System system;
    private LocalDate dateTo;
    private LocalDate dateFrom;
    private BigDecimal amount;
    private PayOff payOff;
    private boolean valid;

    public Long getContractId() {
        return contractId;
    }

    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }

    public Integer getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(Integer contractNumber) {
        this.contractNumber = contractNumber;
    }

    public System getSystem() {
        return system;
    }

    public void setSystem(System system) {
        this.system = system;
    }

    public LocalDate getDateTo() {
        return dateTo;
    }

    public void setDateTo(LocalDate dateTo) {
        this.dateTo = dateTo;
    }

    public LocalDate getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(LocalDate dateFrom) {
        this.dateFrom = dateFrom;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public PayOff getPayOff() {
        return payOff;
    }

    public void setPayOff(PayOff payOff) {
        this.payOff = payOff;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }
}
