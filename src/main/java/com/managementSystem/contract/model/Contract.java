package com.managementSystem.contract.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
//@Table(name= "CONTRACT")
public class Contract {
    @Id
    @GeneratedValue
    @NotNull
    private Long contractId;
    private Integer contractNumber;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "system_id")
    private System system;
    private LocalDate dateTo;
    private LocalDate dateFrom;
    private BigDecimal amount;
    private PayOff payOff;
    private boolean valid;
}


