package com.managementSystem.contract.model;

public enum PayOff {
    MONTHLY, QUARTERLY, ANNUAL, NONE
}
