package com.managementSystem.contract.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
//@Table(name = "SYSTEM")
@Builder
public class System {
    @Id
    @GeneratedValue
    @NotNull
    //@Column( name = "system_id", unique = true)
    private Long systemId;
    private String name;
    private String description;
    private String technologyDescription;
    private String client;


}

