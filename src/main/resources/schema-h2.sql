create table System (
system_id VARCHAR(50),
name VARCHAR(50),
description TEXT,
technology_description VARCHAR(50),
client VARCHAR(50)
);

create table Contract (
	contract_id VARCHAR(50),
	contract_number INT,
	date_to DATE,
	date_from DATE,
	amount INT,
	pay_off VARCHAR(50),
	valid VARCHAR(50),
	system_id INT
);